#!/usr/bin/env sh
mydir=`pwd`/`dirname $0` 
if [ -d ~/.share_xmpp ];
then 
    cd ~/.share_xmpp
else
    mkdir ~/.share_xmpp
fi
python3 ${mydir}/share_gui.py
