# share_xmpp
#基于xmpp的文件分享系统设想

##一、 系统构架

*目标*： 登录同一个服务器的用户之间可以分享本人指定的文件，一个客户端还可以同时登录多个服务器，
并同时向所有服务器发送搜索请求。

*总体结构*： 一个XMPP服务器 + 一个SRV机器人 + 多个客户端