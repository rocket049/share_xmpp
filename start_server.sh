#!/usr/bin/env sh

cd share_pjabberd
python2 pjabberd.py &
pid=$!
echo start jabberd PID: ${pid}
echo kill ${pid} >stop.sh
sleep 1
cd ../share_robot
python3 srv_bot.py &
pid=$!
echo start robot PID: ${pid}
echo kill ${pid} >stop.sh
sleep 1

